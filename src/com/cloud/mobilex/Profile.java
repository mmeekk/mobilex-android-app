package com.cloud.mobilex;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.parse.ParseUser;

public class Profile extends Activity {

	final Context context = this;
	TextView name, username, mail, shortName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);

		getViews();
	}

	private void getViews() {
		name = (TextView) findViewById(R.id.profileName);
		username = (TextView) findViewById(R.id.profileUsername);
		mail = (TextView) findViewById(R.id.profileMail);
		shortName = (TextView) findViewById(R.id.exhibitionFLName);

		name.setText((String) Constants.user.get("Name") + " " + (String) Constants.user.get("Surname"));
		username.setText("/" + (String) Constants.user.getUsername());
		mail.setText((String) Constants.user.getEmail());
		String result = ((String) Constants.user.get("Name")).substring(0, 1);
		result += ((String) Constants.user.get("Surname")).substring(0, 1);
		shortName.setText(result);

		mail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto",
						mail.getText().toString(), null));
				startActivity(Intent.createChooser(emailIntent, "Send email"));
			}
		});
	}

	@Override
	public void onBackPressed() {
		Constants.user = null;
		System.out.println("onbackpressed");
		super.onBackPressed();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch (item.getItemId()) {
		case R.id.action_settings:
			intent = new Intent(context, Settings.class);
			startActivity(intent);
			break;
		case R.id.action_logout:
			ParseUser.logOut();
			intent = new Intent(context, Login.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;

		default:
			return super.onOptionsItemSelected(item);
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem refresh = menu.findItem(R.id.action_refresh);
		refresh.setVisible(false);
		return super.onPrepareOptionsMenu(menu);
	}

}
