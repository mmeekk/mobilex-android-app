package com.cloud.mobilex;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cloud.mobilex.adapter.HomeAdapter;
import com.cloud.mobilex.model.Comment;
import com.cloud.mobilex.model.Exhibition;
import com.cloud.mobilex.model.Painting;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class Home extends Activity {

	final Context context = this;
	ArrayList<Comment> allComments;
	ArrayList<Exhibition> allExhibitions;
	ProgressBar homeProgress;
	TextView info;
	ListView exhibitionList;
	Button addExhibition;
	boolean isCreated;
	static Exhibition exhibition;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		Parse.initialize(context, "kJCgPOAzdrnkeqpOjFWDiPv3rFjumyyGe3MgaS1v",
				"gN4I6eBpTemJchMO8aXBIw8ggkaLAjq1Pu309ynW");
		ParseObject.registerSubclass(Exhibition.class);
		ParseObject.registerSubclass(Painting.class);
		ParseObject.registerSubclass(Comment.class);

		isCreated = true;
		Constants.paintings = new ArrayList<Painting>();
		setActionsForViews();
		getExhibitions();
	}

	private void setActionsForViews() {
		addExhibition = (Button) findViewById(R.id.addExhibition);
		if (!ParseUser.getCurrentUser().get("UserType").equals("Artist")) {
			addExhibition.setVisibility(View.GONE);
		} else {
			addExhibition.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					Intent intent = new Intent(context, AddExhibition.class);
					startActivity(intent);
				}
			});
		}

		homeProgress = (ProgressBar) findViewById(R.id.homeProgress);
		info = (TextView) findViewById(R.id.info);
		info.setVisibility(View.INVISIBLE);
		exhibitionList = (ListView) findViewById(R.id.exhitibitonList);
		exhibitionList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long arg3) {
				getExhibitionsPaintings(context, allExhibitions.get(position));
				exhibition = allExhibitions.get(position);
			}
		});
	}

	public void getExhibitions() {
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Exhibition");
		query.addDescendingOrder("endDate");
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> exhibitions, ParseException e) {
				if (e == null) {
					exhibitionList.setVisibility(View.VISIBLE);
					allExhibitions = new ArrayList<Exhibition>();
					homeProgress.setVisibility(View.INVISIBLE);
					for (ParseObject exhibition : exhibitions)
						allExhibitions.add((Exhibition) exhibition);
					if (allExhibitions.isEmpty()) {
						info.setVisibility(View.VISIBLE);
						homeProgress.setVisibility(View.INVISIBLE);
					} else {
						HomeAdapter adapter = new HomeAdapter(context, allExhibitions);
						exhibitionList.setAdapter(adapter);
						info.setVisibility(View.INVISIBLE);
					}
				} else {
					info.setVisibility(View.VISIBLE);
					Toast.makeText(context, "Error getting exhibitions " + e.getMessage(), Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	public void getExhibitionsPaintings(final Context context, final Exhibition exhibition) {
		final ProgressDialog dialog = new ProgressDialog(context);
		dialog.setMessage("Getting paintings");
		dialog.setCancelable(false);
		dialog.show();

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Painting");
		query.whereEqualTo("exhibition", exhibition);
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> paintings, ParseException e) {
				dialog.dismiss();
				if (e == null) {
					Constants.paintings = new ArrayList<Painting>();
					for (ParseObject painting : paintings) {
						Constants.paintings.add((Painting) painting);
					}
					Intent intent = new Intent(context, ExhibitionContent.class);
					Constants.MODE = Constants.EX_MODE;
					startActivity(intent);
				} else {
					Toast.makeText(context, "Error getting paintings: " + e.getMessage(), Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (isCreated == false)
			getExhibitions();
	}

	@Override
	protected void onPause() {
		super.onPause();
		isCreated = false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch (item.getItemId()) {
		case R.id.action_settings:
			intent = new Intent(context, Settings.class);
			startActivity(intent);
			break;
		case R.id.action_logout:
			ParseUser.logOut();
			intent = new Intent(context, Login.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;

		case R.id.action_refresh:
			exhibitionList.setVisibility(View.INVISIBLE);
			homeProgress.setVisibility(View.VISIBLE);
			getExhibitions();
			break;

		default:
			return super.onOptionsItemSelected(item);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}
}
