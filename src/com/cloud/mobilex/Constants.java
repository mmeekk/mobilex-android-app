package com.cloud.mobilex;

import java.util.ArrayList;

import com.cloud.mobilex.model.Painting;
import com.parse.ParseUser;

public class Constants {

	final public static int EX_MODE = 1;
	final public static int ARTIST_MODE = 2;
	final public static int OWNER_MODE = 3;
	public static ParseUser user = null;
	public static ArrayList<Painting> paintings;
	public static int MODE;
	public static String artistName;
}
