package com.cloud.mobilex;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cloud.mobilex.model.Painting;
import com.parse.ParseFile;
import com.parse.ParseUser;

public class AddPainting extends Activity {

	final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	final static int IMAGE_FROM_GALLERY_CODE = 200;
	final Context context = this;
	final Calendar c = Calendar.getInstance();
	EditText paintingName, artistName, price, description;
	TextView date, tapToAddPhoto;
	AspectRatioImageView takenPhoto;
	ImageButton addPainting, done;
	int startYear, startMonth, startDay;
	Painting painting;
	ParseFile photo;
	Uri fileUri;
	ArrayList<Painting> paintings;
	ArrayList<ImageView> paintingImages;
	String from;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_painting);

		from = getIntent().getExtras().getString("from");
		
		paintings = AddExhibition.paintings;
		paintingImages = AddExhibition.paintingImages;

		startYear = c.get(Calendar.YEAR);
		startMonth = c.get(Calendar.MONTH);
		startDay = c.get(Calendar.DAY_OF_MONTH);

		getViews();
		setActionsForViews();
	}

	private void setActionsForViews() {
		takenPhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				CharSequence[] choicesArray = { "Pick from Gallery", "Take a Photo" };
				builder.setItems(choicesArray, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which == 0) {
							Intent i = new Intent(Intent.ACTION_PICK,
									android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
							startActivityForResult(i, IMAGE_FROM_GALLERY_CODE);
						} else {
							fileUri = getOutputMediaFileUri();
							Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
							intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
							startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
						}
					}
				});
				AlertDialog dialog = builder.create();
				dialog.show();
			}
		});

		addPainting.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (paintingName.getText().toString().equals("") || artistName.getText().toString().equals("")
						|| price.getText().toString().equals("") || description.getText().toString().equals("")) {
					Toast.makeText(context, R.string.fill_all_fields, Toast.LENGTH_LONG).show();
				} else if (takenPhoto.getTag().equals("null")) {
					Toast.makeText(context, R.string.null_photo, Toast.LENGTH_LONG).show();
				} else if (date.getTag().equals("null")) {
					Toast.makeText(context, R.string.null_date, Toast.LENGTH_LONG).show();
				} else {
					painting = new Painting();
					painting.setName(paintingName.getText().toString());
					painting.setArtistName(artistName.getText().toString());
					painting.setOwner(ParseUser.getCurrentUser());
					painting.setPrice(price.getText().toString());
					painting.setDescription(description.getText().toString());
					painting.setDate(date.getText().toString());
					if(from.equals("grid") && photo == null)
						painting.setPhoto(AddExhibition.thePainting.getPhoto());
					else
						painting.setPhoto(photo);
					paintings.add(painting);
					ImageView temp = new ImageView(context);
					temp.setImageDrawable(takenPhoto.getDrawable());
					paintingImages.add(temp);
					Toast.makeText(context, "Painting " + paintingName.getText().toString() + " is saved",
							Toast.LENGTH_SHORT).show();

					takenPhoto.setImageDrawable(context.getResources().getDrawable(R.drawable.taptoaddphoto));
					takenPhoto.setTag("null");
					paintingName.setText("");
					artistName.setText("");
					price.setText("");
					description.setText("");
					date.setText("DD.MM.YYYY");
					date.setTag("null");

					startYear = c.get(Calendar.YEAR);
					startMonth = c.get(Calendar.MONTH);
					startDay = c.get(Calendar.DAY_OF_MONTH);
					
					paintingName.requestFocus();
				}
			}
		});

		date.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				DatePickerDialog datePicker = new DatePickerDialog(context, new OnDateSetListener() {

					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
						startYear = year;
						startMonth = monthOfYear;
						startDay = dayOfMonth;
						date.setText(startDay + "." + (startMonth + 1) + "." + startYear);
						date.setTag("full");
					}
				}, startYear, startMonth, startDay);
				datePicker.show();
			}
		});

		done.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(context).setTitle("Warning")
						.setMessage("Do you want to skip this painting?")
						.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								finish();
							}
						}).setNegativeButton("No", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								Toast.makeText(context, R.string.fill_all_fields, Toast.LENGTH_LONG).show();
								dialog.dismiss();
							}
						});
				AlertDialog dialog = builder.create();

				if (paintingName.getText().toString().equals("") || artistName.getText().toString().equals("")
						|| price.getText().toString().equals("") || description.getText().toString().equals("")) {
					dialog.show();
				} else if (takenPhoto.getTag().equals("null")) {
					dialog.show();
				} else if (date.getTag().equals("null")) {
					dialog.show();
				} else {
					painting = new Painting();
					painting.setName(paintingName.getText().toString());
					painting.setArtistName(artistName.getText().toString());
					painting.setOwner(ParseUser.getCurrentUser());
					painting.setPrice(price.getText().toString());
					painting.setDescription(description.getText().toString());
					painting.setDate(date.getText().toString());
					if(from.equals("grid") && photo == null)
						painting.setPhoto(AddExhibition.thePainting.getPhoto());
					else
						painting.setPhoto(photo);
					paintings.add(painting);
					ImageView temp = new ImageView(context);
					temp.setImageDrawable(takenPhoto.getDrawable());
					paintingImages.add(temp);
					finish();
				}
			}
		});

	}

	private void getViews() {
		paintingName = (EditText) findViewById(R.id.paintingName);
		artistName = (EditText) findViewById(R.id.artistName);
		price = (EditText) findViewById(R.id.price);
		description = (EditText) findViewById(R.id.description);
		date = (TextView) findViewById(R.id.dateText);
//		tapToAddPhoto = (TextView) findViewById(R.id.addPhotoText);
		takenPhoto = (AspectRatioImageView) findViewById(R.id.takenPhoto);
		addPainting = (ImageButton) findViewById(R.id.add);
		done = (ImageButton) findViewById(R.id.done);

		takenPhoto.setTag("null");
		date.setTag("null");
		
		if(from.equals("grid")){
			paintingName.setText(AddExhibition.thePainting.getName());
			artistName.setText(AddExhibition.thePainting.getArtistName());
			price.setText(AddExhibition.thePainting.getPrice());
			description.setText(AddExhibition.thePainting.getDescription());
			date.setText(AddExhibition.thePainting.getDate());
			takenPhoto.setImageDrawable(AddExhibition.theImageView.getDrawable());
			
			takenPhoto.setTag("full");
			date.setTag("full");
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				Uri imageUri = fileUri;
				Bitmap takenPhotoBitmap = null;
				try {
					takenPhotoBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				if (takenPhotoBitmap != null) {
					takenPhotoBitmap = Bitmap.createScaledBitmap(takenPhotoBitmap, takenPhotoBitmap.getWidth() / 4,
							takenPhotoBitmap.getHeight() / 4, true);
					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					takenPhotoBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
					byte[] bitmapData = stream.toByteArray();
					photo = new ParseFile(bitmapData);
					takenPhoto.setImageBitmap(takenPhotoBitmap);
					takenPhoto.setTag("full");
				}
			} else if (resultCode == RESULT_CANCELED) {
			}
		} else if (requestCode == IMAGE_FROM_GALLERY_CODE) {
			if (resultCode == RESULT_OK) {
				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };

				Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				String picturePath = cursor.getString(columnIndex);
				cursor.close();

				Bitmap takenPhotoBitmap = BitmapFactory.decodeFile(picturePath);
				takenPhotoBitmap = Bitmap.createScaledBitmap(takenPhotoBitmap, takenPhotoBitmap.getWidth() / 4,
						takenPhotoBitmap.getHeight() / 4, true);
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				takenPhotoBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
				byte[] bitmapData = stream.toByteArray();
				photo = new ParseFile(bitmapData);
				takenPhoto.setImageBitmap(takenPhotoBitmap);
				takenPhoto.setTag("full");
			} else if (resultCode == RESULT_CANCELED) {
			}
		}
	}

	private static Uri getOutputMediaFileUri() {
		return Uri.fromFile(getOutputMediaFile());
	}

	/** Create a File for saving an image or video */
	private static File getOutputMediaFile() {

		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"Mobilex");

		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				return null;
			}
		}

		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");

		return mediaFile;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch (item.getItemId()) {
		case R.id.action_settings:
			intent = new Intent(context, Settings.class);
			startActivity(intent);
			break;
		case R.id.action_logout:
			ParseUser.logOut();
			intent = new Intent(context, Login.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;

		default:
			return super.onOptionsItemSelected(item);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		AlertDialog.Builder builder = new AlertDialog.Builder(context).setTitle("Warning")
				.setMessage("Do you want to skip this painting?")
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				}).setNegativeButton("No", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Toast.makeText(context, R.string.fill_all_fields, Toast.LENGTH_LONG).show();
						dialog.dismiss();
					}
				});
		AlertDialog dialog = builder.create();

		if (paintingName.getText().toString().equals("") || artistName.getText().toString().equals("")
				|| price.getText().toString().equals("") || description.getText().toString().equals("")) {
			dialog.show();
		} else if (takenPhoto.getTag().equals("null")) {
			dialog.show();
		} else if (date.getTag().equals("null")) {
			dialog.show();
		} else {
			painting = new Painting();
			painting.setName(paintingName.getText().toString());
			painting.setArtistName(artistName.getText().toString());
			painting.setOwner(ParseUser.getCurrentUser());
			painting.setPrice(price.getText().toString());
			painting.setDescription(description.getText().toString());
			painting.setDate(date.getText().toString());
			if(from.equals("grid") && photo == null)
				painting.setPhoto(AddExhibition.thePainting.getPhoto());
			else
				painting.setPhoto(photo);
			paintings.add(painting);
			ImageView temp = new ImageView(context);
			temp.setImageDrawable(takenPhoto.getDrawable());
			paintingImages.add(temp);
			finish();
		}
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem refresh = menu.findItem(R.id.action_refresh);
		refresh.setVisible(false);
		return super.onPrepareOptionsMenu(menu);
	}
}
