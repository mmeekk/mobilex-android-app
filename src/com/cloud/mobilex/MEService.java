package com.cloud.mobilex;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.cloud.mobilex.model.Comment;
import com.cloud.mobilex.model.Exhibition;
import com.cloud.mobilex.model.Painting;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

public class MEService {

	public static void savePainting(final Context context, final Painting painting) {
		final ProgressDialog dialog = new ProgressDialog(context);
		dialog.setMessage("Uploading painting content");
		dialog.show();
		painting.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if (e == null) {
					dialog.dismiss();
					Toast.makeText(context,
							"Painting " + painting.getName() + " saved to the exhibition " + painting.getExhibition().getName(),
							Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(context, "Error saving painting " + e.getMessage(), Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	public static ArrayList<Painting> getExhibitionsPaintings(final Context context, final Exhibition exhibition) {
		final ArrayList<Painting> allPaintingsOfExhibition = new ArrayList<Painting>();
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Painting");
		query.whereEqualTo("exhibition", exhibition);
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> paintings, ParseException e) {
				if (e == null) {
					for (ParseObject painting : paintings) {
						allPaintingsOfExhibition.add((Painting) painting);
					}
				} else {
					Toast.makeText(context, "Error getting paintings: " + e.getMessage(), Toast.LENGTH_SHORT).show();
				}
			}
		});
		return allPaintingsOfExhibition;
	}

	public static void saveCommentsForPainting(final Context context, final Comment comment) {
		comment.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if (e == null) {
					Toast.makeText(context, "Comment saved", Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(context, "Error saving comment: " + e.getMessage(), Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	public static ArrayList<Comment> getCommentsForPainting(final Context context, final Painting painting) {
		final ArrayList<Comment> commentsForPainting = new ArrayList<Comment>();
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Comment");
		query.whereEqualTo("painting", painting);
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> comments, ParseException e) {
				if (e == null) {
					for (ParseObject comment : comments) {
						commentsForPainting.add((Comment) comment);
					}
				} else {
					Toast.makeText(context, "Error getting comments: " + e.getMessage(), Toast.LENGTH_SHORT).show();
				}
			}
		});
		return commentsForPainting;
	}

	public static void saveExhibition(final Context context, final Exhibition exhibition) {
		final ProgressDialog dialog = new ProgressDialog(context);
		dialog.setMessage("Uploading exhibition content");
		dialog.show();
		exhibition.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				dialog.dismiss();
				if (e == null) {
					Toast.makeText(context, "Exhibition " + exhibition.getName() + " is saved", Toast.LENGTH_SHORT)
							.show();
				} else {
					Toast.makeText(context, "Error saving exhibition " + exhibition.getName() + e.getMessage(),
							Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	public static ArrayList<Exhibition> getExhibitions(final Context context) {
		final ArrayList<Exhibition> allExhibitions = new ArrayList<Exhibition>();
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Exhibition");
		query.setLimit(10);
		query.addAscendingOrder("createdAt");
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> exhibitions, ParseException e) {
				if (e == null) {
					for (ParseObject exhibition : exhibitions)
						allExhibitions.add((Exhibition) exhibition);
				} else {
					Toast.makeText(context, "Error getting exhibitions " + e.getMessage(), Toast.LENGTH_SHORT).show();
				}
			}
		});
		return allExhibitions;
	}

}
