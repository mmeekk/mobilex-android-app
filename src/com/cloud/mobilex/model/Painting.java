package com.cloud.mobilex.model;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

@ParseClassName("Painting")
public class Painting extends ParseObject {

	public Painting() {
	}

	public String getPrice() {
		return getString("price");
	}

	public void setPrice(String price) {
		put("price", price);
	}

	public String getDate() {
		return getString("date");
	}

	public void setDate(String date) {
		put("date", date);
	}

	public String getName() {
		return getString("name");
	}

	public void setName(String name) {
		put("name", name);
	}

	public String getArtistName() {
		return getString("artistName");
	}

	public void setArtistName(String artistName) {
		put("artistName", artistName);
	}

	public String getDescription() {
		return getString("description");
	}

	public void setDescription(String description) {
		put("description", description);
	}

	public Exhibition getExhibition() {
		return (Exhibition) getParseObject("exhibition");
	}

	public void setExhibiton(Exhibition exhibiton) {
		put("exhibition", exhibiton);
	}

	public ParseFile getPhoto() {
		return getParseFile("photo");
	}

	public void setPhoto(ParseFile photo) {
		put("photo", photo);
	}

	public ParseUser getOwner() {
		return getParseUser("owner");
	}

	public void setOwner(ParseUser owner) {
		put("owner", owner);
	}
}
