package com.cloud.mobilex.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("Exhibition")
public class Exhibition extends ParseObject {

	public Exhibition() {
	}

	public String getName() {
		return getString("name");
	}

	public void setName(String name) {
		put("name", name);
	}

	public String getStartDate() {
		return getString("startDate");
	}

	public void setStartDate(String startDate) {
		put("startDate", startDate);
	}

	public String getEndDate() {
		return getString("endDate");
	}

	public void setEndDate(String endDate) {
		put("endDate", endDate);
	}

	public String getPlace() {
		return getString("place");
	}

	public void setPlace(String place) {
		put("place", place);
	}

	public int getPaintingCount() {
		return getInt("paintingCount");
	}

	public void setPaintingCount(int count) {
		put("paintingCount", count);
	}

	public String getOwner() {
		return getString("owner");
	}

	public void setOwner(String owner) {
		put("owner", owner);
	}

	public String getOwnerNameSurname() {
		return getString("ownerNameSurname");
	}

	public void setOwnerNameSurname(String ownerNameSurname) {
		put("ownerNameSurname", ownerNameSurname);
	}
}
