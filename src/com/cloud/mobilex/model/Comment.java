package com.cloud.mobilex.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

@ParseClassName("Comment")
public class Comment extends ParseObject {

	String date;
	String comment;
	ParseUser commenter;
	Painting commentedPainting;

	public Comment() {
	}

	public String getComment() {
		return getString("comment");
	}

	public void setComment(String comment) {
		put("comment", comment);
	}
	
	public String getCommenterName(){
		return getString("commenterName");
	}
	
	public void setCommenterName(String name){
		put("commenterName", name);
	}

	public ParseUser getCommenter() {
		return getParseUser("commenter");
	}

	public void setCommenter(ParseUser commentOwner) {
		put("commenter", commentOwner);
	}

	public Painting getCommentedPainting() {
		return (Painting) getParseObject("commentedPainting");
	}

	public void setCommentedPainting(Painting painting) {
		put("commentedPainting", painting);
	}

	@Override
	public String toString() {
		return "Comment [comment=" + getComment() + ", commenter=" + getCommenter()
				+ ", commentedPainting=" + getCommentedPainting() + "]";
	}

}
