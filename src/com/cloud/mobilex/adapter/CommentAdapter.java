package com.cloud.mobilex.adapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.cloud.mobilex.ExhibitionContent;
import com.cloud.mobilex.R;
import com.cloud.mobilex.lib.CircleFlowIndicator;
import com.cloud.mobilex.lib.ViewFlow;
import com.cloud.mobilex.lib.ViewFlow.ViewSwitchListener;
import com.cloud.mobilex.model.Comment;
import com.cloud.mobilex.model.Painting;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class CommentAdapter extends BaseAdapter {

	final int COMMENT_ITEM_TYPE = 0;
	final int PHOTO_ITEM_TYPE = 1;
	static ArrayList<Comment> realComments;
	ArrayList<Painting> paintings;
	LayoutInflater mInflater;
	Context context;

	static class ViewHolder {
		TextView commenter;
		TextView comment;
		TextView date;
	}

	public CommentAdapter(Context context, ArrayList<Painting> paintings, ArrayList<Comment> comments) {
		this.paintings = paintings;
		CommentAdapter.realComments = comments;
		this.context = context;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return realComments.size() + 1;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getItemViewType(int position) {
		if (position == 0)
			return PHOTO_ITEM_TYPE;
		else
			return COMMENT_ITEM_TYPE;
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		int type = getItemViewType(position);
		if (type == PHOTO_ITEM_TYPE) {
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.viewflow_list_item, null);

				ViewFlow paintingViewFlow = (ViewFlow) convertView.findViewById(R.id.paintingViewFlow);
				CircleFlowIndicator indicator = (CircleFlowIndicator) convertView
						.findViewById(R.id.circleFlowIndicator);
				paintingViewFlow.setAdapter(new PaintingAdapter(context, paintings));
				paintingViewFlow.setFlowIndicator(indicator);

				paintingViewFlow.setOnViewSwitchListener(new ViewSwitchListener() {

					@Override
					public void onSwitched(View view, int position) {
						getCommentsForPainting(context, paintings.get(position), view);
						ExhibitionContent.whichPainting = position;
						System.out.println("WHICH = " + ExhibitionContent.whichPainting);
					}
				});
			}

		} else if (type == COMMENT_ITEM_TYPE) {
			ViewHolder viewHolder;
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.viewflow_comment_item, null);
				viewHolder = new ViewHolder();

				viewHolder.comment = (TextView) convertView.findViewById(R.id.commentContent);
				viewHolder.commenter = (TextView) convertView.findViewById(R.id.commenterName);
				viewHolder.date = (TextView) convertView.findViewById(R.id.commentDate);

				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}

			position--;
			viewHolder.comment.setText(realComments.get(position).getComment());
			viewHolder.commenter.setText(realComments.get(position).getCommenterName());
			Date date = realComments.get(position).getCreatedAt();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			viewHolder.date.setText(calendar.get(Calendar.DAY_OF_MONTH) + "." + calendar.get(Calendar.MONTH) + "."
					+ calendar.get(Calendar.YEAR) + " " + calendar.get(Calendar.HOUR_OF_DAY) + ":"
					+ calendar.get(Calendar.MINUTE));
		}
		return convertView;
	}

	public void getCommentsForPainting(final Context context, final Painting painting, View view) {
		realComments = new ArrayList<Comment>();
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Comment");
		query.whereEqualTo("commentedPainting", painting);
		query.addDescendingOrder("createdAt");
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> comments, ParseException e) {
				if (e == null) {
					for (ParseObject comment : comments) {
						realComments.add((Comment) comment);
					}
					System.out.println("comment size = " + comments.size());
					for (Comment comment : realComments)
						System.out.println(comment.toString());
					notifyDataSetChanged();
				} else {
					Toast.makeText(context, "Error getting comments: " + e.getMessage(), Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	public static void setComments(ArrayList<Comment> comments) {
		realComments = new ArrayList<Comment>();
		realComments = comments;
	}
	
	public ArrayList<Comment> getComments(){
		return realComments;
	}

}
