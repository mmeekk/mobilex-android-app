package com.cloud.mobilex.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.cloud.mobilex.AspectRatioImageView;
import com.cloud.mobilex.Constants;
import com.cloud.mobilex.ExhibitionContent;
import com.cloud.mobilex.R;
import com.cloud.mobilex.model.Painting;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class PaintingAdapter extends BaseAdapter {

	ArrayList<Painting> paintings;
	LayoutInflater mInflater;
	Context context;

	public PaintingAdapter(Context context, ArrayList<Painting> paintings) {
		this.paintings = paintings;
		this.context = context;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return paintings.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.viewflow_painting_item, null);
		}

		final AspectRatioImageView photo = (AspectRatioImageView) convertView.findViewById(R.id.listPaintingItem);
		ParseFile file = paintings.get(position).getParseFile("photo");
		file.getDataInBackground(new GetDataCallback() {

			@Override
			public void done(byte[] data, ParseException e) {
				if (e == null) {
					Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
					photo.setImageBitmap(bmp);
				} else
					System.out.println(e.getMessage());
			}
		});

		TextView artistName = (TextView) convertView.findViewById(R.id.paintingItemArtistName);
		TextView paintingName = (TextView) convertView.findViewById(R.id.paintingItemPaintingName);
		TextView price = (TextView) convertView.findViewById(R.id.paintingItemPrice);
		TextView desc = (TextView) convertView.findViewById(R.id.paintingItemDesc);
		TextView date = (TextView) convertView.findViewById(R.id.paintingItemDate);

		artistName.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				TextView text = (TextView) v;
				Constants.artistName = text.getText().toString();
				getArtistPaintings(context, Constants.artistName);
			}
		});

		artistName.setText(paintings.get(position).getArtistName());
		paintingName.setText(paintings.get(position).getName());
		price.setText(paintings.get(position).getPrice());
		desc.setText(paintings.get(position).getDescription());
		date.setText(paintings.get(position).getDate());

		return convertView;
	}

	public void getArtistPaintings(final Context context, final String artistName) {
		final ProgressDialog dialog = new ProgressDialog(context);
		dialog.setMessage("Getting paintings of artist");
		dialog.setCancelable(false);
		dialog.show();

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Painting");
		query.whereEqualTo("artistName", artistName);
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> paintings, ParseException e) {
				dialog.dismiss();
				if (e == null) {
					Constants.paintings = new ArrayList<Painting>();
					for (ParseObject painting : paintings) {
						Constants.paintings.add((Painting) painting);
					}
					Intent intent = new Intent(context, ExhibitionContent.class);
					Constants.MODE = Constants.ARTIST_MODE;
					context.startActivity(intent);
				} else {
					Toast.makeText(context, "Error getting paintings: " + e.getMessage(), Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
}
