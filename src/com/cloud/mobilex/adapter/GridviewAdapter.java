package com.cloud.mobilex.adapter;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.cloud.mobilex.R;
import com.cloud.mobilex.model.Painting;

public class GridviewAdapter extends BaseAdapter {

	ArrayList<Painting> paintings;
	ArrayList<ImageView> paintingImages;
	LayoutInflater mInflater;
	Context context;

	static class ViewHolder {
		ImageView painting;
		ImageView delete;
	}

	public GridviewAdapter(Context context, ArrayList<Painting> paintings, ArrayList<ImageView> paintingImages) {
		this.paintings = paintings;
		this.paintingImages = paintingImages;
		this.context = context;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return paintings.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.gridview_item, null);
			viewHolder = new ViewHolder();

			viewHolder.painting = (ImageView) convertView.findViewById(R.id.paintingItem);
			viewHolder.delete = (ImageView) convertView.findViewById(R.id.deletePainting);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		viewHolder.painting.setImageDrawable(paintingImages.get(position).getDrawable());

		viewHolder.delete.bringToFront();
		viewHolder.delete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				AlertDialog.Builder builder = new AlertDialog.Builder(context).setTitle("Warning")
						.setMessage("Do you want to delete this painting?")
						.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								paintingImages.remove(position);
								paintings.remove(position);
								notifyDataSetChanged();
							}
						}).setNegativeButton("No", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								
							}
						});
				AlertDialog dialog = builder.create();
				dialog.show();
			}
		});

		return convertView;
	}
}
