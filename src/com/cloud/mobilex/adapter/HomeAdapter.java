package com.cloud.mobilex.adapter;

import java.util.ArrayList;
import java.util.StringTokenizer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cloud.mobilex.R;
import com.cloud.mobilex.model.Exhibition;

public class HomeAdapter extends BaseAdapter {

	ArrayList<Exhibition> allExhibitions;
	LayoutInflater mInflater;
	Context context;

	public class ViewHolder {
		TextView name;
		TextView owner;
		TextView date;
		TextView place;
		TextView paintingCount;
		ImageView suitcase;
	}

	public HomeAdapter(Context context, ArrayList<Exhibition> allExhibitions) {
		this.allExhibitions = allExhibitions;
		this.context = context;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return allExhibitions.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.home_list_item, null);
			viewHolder = new ViewHolder();

			viewHolder.name = (TextView) convertView.findViewById(R.id.listExhibitionName);
			viewHolder.date = (TextView) convertView.findViewById(R.id.listDate);
			viewHolder.paintingCount = (TextView) convertView.findViewById(R.id.listCount);
			viewHolder.owner = (TextView) convertView.findViewById(R.id.ownerFLName);
			viewHolder.place = (TextView) convertView.findViewById(R.id.listPlace);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		Exhibition ex = allExhibitions.get(position);
		String ownerNameSurname = ex.getOwnerNameSurname();
		StringTokenizer tokenizer = new StringTokenizer(ownerNameSurname);
		String result = "";
		while (tokenizer.hasMoreElements()) {
			String str = (String) tokenizer.nextElement();
			result += str.substring(0, 1);
		}
		viewHolder.owner.setText(result);
		viewHolder.date.setText(ex.getStartDate() + " - " + ex.getEndDate());
		viewHolder.place.setText(ex.getPlace());
		viewHolder.paintingCount.setText(ex.getPaintingCount() + "");
		viewHolder.name.setText(ex.getName());

		return convertView;
	}

}
