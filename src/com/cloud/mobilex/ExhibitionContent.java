package com.cloud.mobilex;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cloud.mobilex.adapter.CommentAdapter;
import com.cloud.mobilex.model.Comment;
import com.cloud.mobilex.model.Painting;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class ExhibitionContent extends Activity {

	final Context context = this;
	ProgressDialog dialog;
	ListView contentList;
	ImageView send, barImage, exOwner;
	TextView firstRow, secondRow, thirdRow, warningMessage, shortName;
	EditText commentText;
	CommentAdapter adapter;
	RelativeLayout ustbar;
	boolean isFirst = true;
	public static int whichPainting = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_exhibition_content);

		getViewsAndSetActions();

	}

	private void getViewsAndSetActions() {
		dialog = new ProgressDialog(context);

		contentList = (ListView) findViewById(R.id.contentList);
		send = (ImageView) findViewById(R.id.commentSend);
		commentText = (EditText) findViewById(R.id.commentEdit);
		exOwner = (ImageView) findViewById(R.id.exhibitionOwner);
		warningMessage = (TextView) findViewById(R.id.warningMessage);

		ustbar = (RelativeLayout) findViewById(R.id.profileBar);
		firstRow = (TextView) ustbar.findViewById(R.id.profileName);
		secondRow = (TextView) ustbar.findViewById(R.id.profileUsername);
		thirdRow = (TextView) ustbar.findViewById(R.id.profileMail);
		shortName = (TextView) ustbar.findViewById(R.id.exhibitionFLName);

		barImage = (ImageView) ustbar.findViewById(R.id.profilePic);

		if (Constants.MODE == Constants.OWNER_MODE) {
			setTitle("Profile");
			firstRow.setText((String) Constants.user.get("Name") + " " + (String) Constants.user.get("Surname"));
			secondRow.setText("/" + (String) Constants.user.getUsername());
			thirdRow.setText((String) Constants.user.getEmail());
			exOwner.setVisibility(View.INVISIBLE);

			thirdRow.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", thirdRow.getText()
							.toString(), null));
					emailIntent.putExtra(Intent.EXTRA_SUBJECT, "About Your Exhibitions");
					startActivity(Intent.createChooser(emailIntent, "Send email"));
				}
			});

			String result = ((String) Constants.user.get("Name")).substring(0, 1);
			result += ((String) Constants.user.get("Surname")).substring(0, 1);
			shortName.setText(result);
		} else if (Constants.MODE == Constants.EX_MODE) {
			firstRow.setText(Home.exhibition.getName());
			secondRow.setText(Home.exhibition.getPlace());
			thirdRow.setText(Home.exhibition.getStartDate() + " - " + Home.exhibition.getEndDate());
			String ownerNameSurname = Home.exhibition.getName();
			StringTokenizer tokenizer = new StringTokenizer(ownerNameSurname);
			String result = "";
			while (tokenizer.hasMoreElements()) {
				String str = (String) tokenizer.nextElement();
				result += str.substring(0, 1);
			}
			shortName.setText(result);
		} else if (Constants.MODE == Constants.ARTIST_MODE) {
			setTitle("Artist");
			firstRow.setText(Constants.artistName);
			secondRow.setText(Constants.paintings.size() + " Paintings");
			thirdRow.setText("");
			exOwner.setVisibility(View.INVISIBLE);
			String ownerNameSurname = Constants.artistName;
			StringTokenizer tokenizer = new StringTokenizer(ownerNameSurname);
			String result = "";
			while (tokenizer.hasMoreElements()) {
				String str = (String) tokenizer.nextElement();
				result += str.substring(0, 1);
			}
			shortName.setText(result);
		}

		if (!Constants.paintings.isEmpty()) {
			getCommentsForPainting(context, Constants.paintings.get(0));

			send.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					if (commentText.getText().toString().length() < 20) {
						Toast.makeText(context, R.string.null_comment, Toast.LENGTH_LONG).show();
					} else {
						Comment comment = new Comment();
						comment.setComment(commentText.getText().toString());
						comment.setCommenter(ParseUser.getCurrentUser());
						comment.setCommenterName(ParseUser.getCurrentUser().getString("Name") + " "
								+ ParseUser.getCurrentUser().getString("Surname"));
						comment.setCommentedPainting(Constants.paintings.get(whichPainting));
						saveCommentsForPainting(context, comment);
						commentText.setText("");
					}
				}
			});

			contentList.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
					if (position != 0) {
						Comment comment = adapter.getComments().get(position - 1);
						getUserProfile(comment.getCommenter().getObjectId());
					}
				}
			});

			exOwner.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					getUserProfileByName(Home.exhibition.getOwner());
				}

			});
		} else {
			send.setVisibility(View.INVISIBLE);
			contentList.setVisibility(View.INVISIBLE);
			commentText.setVisibility(View.INVISIBLE);
			warningMessage.setVisibility(View.VISIBLE);
		}

	}

	private void getUserProfileByName(String owner) {
		dialog.setMessage("Loading profile...");
		dialog.setCancelable(false);
		dialog.show();
		ParseQuery<ParseUser> query = ParseUser.getQuery();
		query.whereEqualTo("email", owner);
		query.getFirstInBackground(new GetCallback<ParseUser>() {

			@Override
			public void done(ParseUser object, ParseException e) {
				if (e == null) {
					Constants.user = (ParseUser) object;
					getUserPaintings(context, Constants.user);
				} else {
					Toast.makeText(context, "Error getting profile: " + e.getMessage(), Toast.LENGTH_SHORT).show();
				}

			}
		});
	}

	public void getCommentsForPainting(final Context context, final Painting painting) {
		final ArrayList<Comment> realComments = new ArrayList<Comment>();
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Comment");
		query.whereEqualTo("commentedPainting", painting);
		query.addDescendingOrder("createdAt");
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> comments, ParseException e) {
				if (e == null) {
					for (ParseObject comment : comments) {
						realComments.add((Comment) comment);
					}
					System.out.println("comment size = " + comments.size());
					for (Comment comment : realComments)
						System.out.println(comment.toString());
					if (isFirst) {
						adapter = new CommentAdapter(context, Constants.paintings, realComments);
						contentList.setAdapter(adapter);
						isFirst = false;
					} else {
						CommentAdapter.setComments(realComments);
						adapter.notifyDataSetChanged();
					}
				} else {
					Toast.makeText(context, "Error getting comments: " + e.getMessage(), Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	public void saveCommentsForPainting(final Context context, final Comment comment) {
		comment.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if (e == null) {
					getCommentsForPainting(context, comment.getCommentedPainting());
				} else {
					Toast.makeText(context, "Error saving comment: " + e.getMessage(), Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	public void getUserProfile(String objectId) {
		dialog.setMessage("Loading profile...");
		dialog.setCancelable(false);
		dialog.show();
		System.out.println(objectId);
		ParseQuery<ParseUser> query = ParseUser.getQuery();
		query.whereEqualTo("objectId", objectId);
		query.getFirstInBackground(new GetCallback<ParseUser>() {

			@Override
			public void done(ParseUser object, ParseException e) {
				if (e == null) {
					Constants.user = (ParseUser) object;
					getUserPaintings(context, Constants.user);
				} else {
					Toast.makeText(context, "Error getting profile: " + e.getMessage(), Toast.LENGTH_SHORT).show();
				}

			}
		});
	}

	public void getUserPaintings(final Context context, final ParseUser user) {
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Painting");
		query.whereEqualTo("owner", user);
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> paintings, ParseException e) {
				dialog.dismiss();
				if (e == null) {
					Constants.paintings = new ArrayList<Painting>();
					for (ParseObject painting : paintings) {
						Constants.paintings.add((Painting) painting);
					}
					Intent intent;
					if (Constants.user.getString("UserType").equals("Artist")) {
						intent = new Intent(context, ExhibitionContent.class);
						Constants.MODE = Constants.OWNER_MODE;
					} else {
						intent = new Intent(context, Profile.class);
					}
					startActivity(intent);
				} else {
					Toast.makeText(context, "Error getting paintings: " + e.getMessage(), Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Constants.user = null;
		System.out.println("onbackpressed");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch (item.getItemId()) {
		case R.id.action_settings:
			intent = new Intent(context, Settings.class);
			startActivity(intent);
			break;
		case R.id.action_logout:
			ParseUser.logOut();
			intent = new Intent(context, Login.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;

		default:
			return super.onOptionsItemSelected(item);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem refresh = menu.findItem(R.id.action_refresh);
		refresh.setVisible(false);
		return super.onPrepareOptionsMenu(menu);
	}

}
