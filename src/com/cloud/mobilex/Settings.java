package com.cloud.mobilex;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Settings extends Activity {

	final Context context = this;
	EditText oldPass, newPass, newPassAgain;
	Button changePass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		System.out.println(ParseUser.getCurrentUser().get("password"));
		System.out.println(ParseUser.getCurrentUser().getUsername());
		
		getViewsAndSetActions();
	}

	private void getViewsAndSetActions() {
		oldPass = (EditText) findViewById(R.id.changePassOld);
		newPass = (EditText) findViewById(R.id.changePassNew);
		newPassAgain = (EditText) findViewById(R.id.changePassNewRe);
		changePass = (Button) findViewById(R.id.changePassButon);

		changePass.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (oldPass.getText().toString().equals("") && newPass.getText().toString().equals("")
						&& newPassAgain.getText().toString().equals("")) {
					Toast.makeText(context, R.string.fill_all_fields, Toast.LENGTH_LONG).show();
				} else if (!newPass.getText().toString().equals(newPassAgain.getText().toString())) {
					Toast.makeText(context, R.string.password_doesnt_match, Toast.LENGTH_LONG).show();
				} else {
					ParseUser.logInInBackground(ParseUser.getCurrentUser().getUsername(), oldPass.getText().toString(),
							new LogInCallback() {

								@Override
								public void done(ParseUser user, ParseException e) {
									if (e == null) {
										user.setPassword(newPass.getText().toString());
										user.saveInBackground();
										Toast.makeText(context, R.string.password_changed, Toast.LENGTH_LONG).show();
										finish();
									} else {
										Toast.makeText(context, R.string.old_password_wrong, Toast.LENGTH_LONG).show();
										System.out.println(e.getMessage());
									}
								}
							});
				}
			}
		});
	}
	
	@Override
	public void onBackPressed() {
		finish();
	}
}
