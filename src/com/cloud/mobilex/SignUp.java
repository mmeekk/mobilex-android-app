package com.cloud.mobilex;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class SignUp extends Activity {

	final Context context = this;
	final Activity activity = this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up);

		Parse.initialize(context, "kJCgPOAzdrnkeqpOjFWDiPv3rFjumyyGe3MgaS1v",
				"gN4I6eBpTemJchMO8aXBIw8ggkaLAjq1Pu309ynW");

		final EditText name = (EditText) findViewById(R.id.signUpName);
		final EditText surname = (EditText) findViewById(R.id.signUpSurname);
		final EditText email = (EditText) findViewById(R.id.signUpEmail);
		final EditText username = (EditText) findViewById(R.id.signUpUsername);
		final EditText password = (EditText) findViewById(R.id.signUpPass);
		final ProgressBar pBar = (ProgressBar) findViewById(R.id.signUpProgress);
		
		final RadioGroup userType = (RadioGroup) findViewById(R.id.userType);
		RadioButton artist = (RadioButton) userType.findViewById(R.id.artist);
		RadioButton regular = (RadioButton) userType.findViewById(R.id.user);
		artist.setTag("Artist");
		regular.setTag("Regular");

		final Button signUp = (Button) findViewById(R.id.signUp);
		signUp.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (!(!username.getText().toString().equals("") && !password.getText().toString().equals("")
						&& !email.getText().toString().equals("") && !name.getText().toString().equals("") && !surname
						.getText().toString().equals(""))) {
					Toast.makeText(context, "Please fill all the fields!", Toast.LENGTH_LONG).show();
				} else {
					pBar.setVisibility(View.VISIBLE);
					signUp.setText("");
					ParseUser user = new ParseUser();
					user.setUsername(username.getText().toString());
					user.setPassword(password.getText().toString());
					user.setEmail(email.getText().toString());

					user.put("Name", name.getText().toString());
					user.put("Surname", surname.getText().toString());
					int id = userType.getCheckedRadioButtonId();
					RadioButton selected = (RadioButton) userType.findViewById(id);
					if (selected.getTag().equals("Artist")) {
						user.put("UserType", "Artist");
					} else {
						user.put("UserType", "Regular");
					}

					user.signUpInBackground(new SignUpCallback() {
						public void done(ParseException e) {
							if (e == null) {
								Toast.makeText(context, "Signup successful", Toast.LENGTH_LONG).show();
								activity.finish();
							} else {
								Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
								signUp.setText("Sign Up");
								pBar.setVisibility(View.INVISIBLE);
							}
						}
					});
				}
			}
		});
	}
}
