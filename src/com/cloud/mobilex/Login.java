package com.cloud.mobilex;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cloud.mobilex.model.Comment;
import com.cloud.mobilex.model.Exhibition;
import com.cloud.mobilex.model.Painting;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

public class Login extends Activity {

	final Context context = this;
	Button login;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		Parse.initialize(context, "kJCgPOAzdrnkeqpOjFWDiPv3rFjumyyGe3MgaS1v",
				"gN4I6eBpTemJchMO8aXBIw8ggkaLAjq1Pu309ynW");
		ParseObject.registerSubclass(Exhibition.class);
		ParseObject.registerSubclass(Painting.class);
		ParseObject.registerSubclass(Comment.class);

		if (ParseUser.getCurrentUser() != null && ParseUser.getCurrentUser().isAuthenticated()) {
			Intent intent = new Intent(context, Home.class);
			finish();
			overridePendingTransition(0, 0);
			startActivity(intent);
		}

		setActionsForViews();

	}

	private void setActionsForViews() {
		final EditText username = (EditText) findViewById(R.id.homeUsername);
		final EditText pass = (EditText) findViewById(R.id.homePass);

		final ProgressBar pBar = (ProgressBar) findViewById(R.id.loginProgress);

		Button signup = (Button) findViewById(R.id.homeSignUp);
		signup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, SignUp.class);
				startActivity(intent);
			}
		});

		login = (Button) findViewById(R.id.homeLogIn);
		login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (username.getText().equals("") || pass.getText().equals("")) {
					Toast.makeText(context, "Please fill all the fields!", Toast.LENGTH_LONG).show();
				} else {
					pBar.setVisibility(View.VISIBLE);
					final Button login = (Button) v;
					login.setText("");
					login.setEnabled(false);
					ParseUser.logInInBackground(username.getText().toString(), pass.getText().toString(),
							new LogInCallback() {
								public void done(ParseUser user, ParseException e) {
									pBar.setVisibility(View.INVISIBLE);
									login.setText("Log In");
									if (user != null) {
										Intent intent = new Intent(context, Home.class);
										finish();
										overridePendingTransition(0, 0);
										startActivity(intent);
									} else {
										login.setEnabled(true);
										Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
									}
								}
							});
				}

			}
		});

	}

	@Override
	protected void onResume() {
		super.onResume();
		login.setEnabled(true);
	}

}
