package com.cloud.mobilex;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.cloud.mobilex.adapter.GridviewAdapter;
import com.cloud.mobilex.model.Exhibition;
import com.cloud.mobilex.model.Painting;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class AddExhibition extends Activity {

	static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	final Context context = this;
	EditText exName, exPlace;
	Button exStartDate, exEndDate, addPainting, publishEx;
	int startYear, startMonth, startDay, endYear, endMonth, endDay, responseCount, responseSoFar;
	DatePickerDialog datePicker;
	GridView paintingsGrid;
	GridviewAdapter adapter;
	static Exhibition exhibition;
	static ArrayList<Painting> paintings;
	static ArrayList<ImageView> paintingImages;
	static Painting thePainting;
	static ImageView theImageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_exhibition);

		paintings = new ArrayList<Painting>();
		paintingImages = new ArrayList<ImageView>();

		final Calendar c = Calendar.getInstance();
		startYear = c.get(Calendar.YEAR);
		startMonth = c.get(Calendar.MONTH);
		startDay = c.get(Calendar.DAY_OF_MONTH);

		setActionsForViews();
	}

	private void setActionsForViews() {

		exName = (EditText) findViewById(R.id.exhibitionName);
		exStartDate = (Button) findViewById(R.id.startDateButton);
		exEndDate = (Button) findViewById(R.id.endDateButton);
		exPlace = (EditText) findViewById(R.id.place);
		paintingsGrid = (GridView) findViewById(R.id.paintingsGrid);
		publishEx = (Button) findViewById(R.id.publishEx);

		exStartDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				datePicker = new DatePickerDialog(context, new OnDateSetListener() {

					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
						startYear = year;
						startMonth = monthOfYear;
						startDay = dayOfMonth;
						exStartDate.setText(startDay + "." + (startMonth + 1) + "." + startYear);
					}
				}, startYear, startMonth, startDay);
				datePicker.show();

			}
		});

		exEndDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				datePicker = new DatePickerDialog(context, new OnDateSetListener() {

					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
						endYear = year;
						endMonth = monthOfYear;
						endDay = dayOfMonth;
						exEndDate.setText(endDay + "." + (endMonth + 1) + "." + endYear);
					}
				}, startYear, startMonth, startDay);
				datePicker.show();
			}
		});

		addPainting = (Button) findViewById(R.id.addPainting);
		addPainting.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, AddPainting.class);
				intent.putExtra("from", "button");
				startActivity(intent);
			}
		});

		publishEx.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (exName.getText().toString().equals("") || exStartDate.getText().toString().equals("")
						|| exEndDate.getText().toString().equals("") || exPlace.getText().toString().equals("")) {
					Toast.makeText(context, R.string.fill_all_fields, Toast.LENGTH_LONG).show();
				} else if (paintings.isEmpty()) {
					Toast.makeText(context, R.string.no_paintings_are_added, Toast.LENGTH_LONG).show();
				} else {
					final ProgressDialog dialog = new ProgressDialog(context);
					dialog.setMessage("Uploading exhibition content");
					dialog.setCancelable(false);
					dialog.show();
					exhibition = new Exhibition();
					exhibition.setName(exName.getText().toString());
					exhibition.setStartDate(exStartDate.getText().toString());
					exhibition.setEndDate(exEndDate.getText().toString());
					exhibition.setPlace(exPlace.getText().toString());
					exhibition.setPaintingCount(paintings.size());
					exhibition.setOwnerNameSurname(ParseUser.getCurrentUser().getString("Name") + " "
							+ ParseUser.getCurrentUser().getString("Surname"));
					exhibition.setOwner(ParseUser.getCurrentUser().getEmail());
					responseCount = paintings.size() * 2 + 1;
					responseSoFar = 0;
					exhibition.saveInBackground(new SaveCallback() {

						@Override
						public void done(ParseException e) {
							if (e == null) {
								responseSoFar++;
							} else {
								Toast.makeText(context, "Error saving painting image " + e.getMessage(),
										Toast.LENGTH_SHORT).show();
								if (dialog.isShowing())
									dialog.dismiss();
							}
						}
					});
					for (int i = 0; i < paintings.size(); i++) {
						paintings.get(i).setExhibiton(exhibition);
						paintings.get(i).getPhoto().saveInBackground(new SaveCallback() {

							@Override
							public void done(ParseException e) {
								if (e == null) {
									responseSoFar++;
									if (responseSoFar == responseCount) {
										Toast.makeText(context, "Exhibition is posted", Toast.LENGTH_SHORT).show();
										dialog.dismiss();
										finish();
									}
								} else {
									Toast.makeText(context, "Error saving painting image " + e.getMessage(),
											Toast.LENGTH_SHORT).show();
									if (dialog.isShowing())
										dialog.dismiss();
								}
							}
						});
						paintings.get(i).saveInBackground(new SaveCallback() {

							@Override
							public void done(ParseException e) {
								if (e == null) {
									responseSoFar++;
									if (responseSoFar == responseCount) {
										Toast.makeText(context, "Exhibition is posted", Toast.LENGTH_SHORT).show();
										dialog.dismiss();
										finish();
									}
								} else {
									Toast.makeText(context, "Error saving painting " + e.getMessage(),
											Toast.LENGTH_SHORT).show();
									if (dialog.isShowing())
										dialog.dismiss();
								}
							}
						});
					}

				}
			}
		});
		
		paintingsGrid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				thePainting = paintings.get(position);
				theImageView = paintingImages.get(position);
				paintings.remove(position);
				paintingImages.remove(position);
				Intent intent = new Intent(context, AddPainting.class);
				intent.putExtra("from", "grid");
				startActivity(intent);
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (!paintings.isEmpty()) {
			adapter = new GridviewAdapter(context, paintings, paintingImages);
			paintingsGrid.setAdapter(adapter);
			System.out.println("adapter is set");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch (item.getItemId()) {
		case R.id.action_settings:
			intent = new Intent(context, Settings.class);
			startActivity(intent);
			break;
		case R.id.action_logout:
			ParseUser.logOut();
			intent = new Intent(context, Login.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;

		default:
			return super.onOptionsItemSelected(item);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem refresh = menu.findItem(R.id.action_refresh);
		refresh.setVisible(false);
		return super.onPrepareOptionsMenu(menu);
	}
}
